﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using TicTacToe.Common;

namespace TicTacToe.Models.Services.Battle.Get
{
    public class BattleResponse
    {
        public string Player1 { get; set; }

        public string Player2 { get; set; }

        public DateTime Date { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public ResultGame Result { get; set; }

    }
}
