﻿using System.Collections.Generic;

namespace TicTacToe.Models.Services.Battle.Get
{
    public class BattlesResponse
    {
        public IEnumerable<BattleResponse> Battles { get; set; }
    }
}
