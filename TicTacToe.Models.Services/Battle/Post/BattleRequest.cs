﻿using System;
using TicTacToe.Common;

namespace TicTacToe.Models.Services.Battle.Post
{
    public class BattleRequest
    {
        public string User1 { get; set; }

        public string User2 { get; set; }

        public DateTime? Date { get; set; }

        public ResultGame Result { get; set; }
    }
}
