﻿using TicTacToe.Common;

namespace TicTacToe.Models.Services.Hubs
{
    public class Battle
    {
        public string _IdBattle { get; set; }

        public string _NamePlayerAsk { get; set; }

        public string _IdPlayerAsk { get; set; }

        public string _NamePlayerCall { get; set; }

        public string _IdplayerCall { get; set; }

        public bool? _Accepted { get; set; }

        public string _ActivePlayer { get; set; }

        public string[,] _Grid { get; } = new string[3, 3];

        public ResultGame? Result { get; set; }

        public string IdActivePlayer()
        {
            return _ActivePlayer.Equals(_NamePlayerAsk) ? _IdPlayerAsk : _IdplayerCall;
        }

        public void UpdateActivePlayer()
        {
            _ActivePlayer = _ActivePlayer.Equals(_NamePlayerAsk) ? _NamePlayerCall : _NamePlayerAsk;
        }

        public bool AddInGrid(int a)
        {
            if (_Grid.GetValue(a) != null)
            {
                _Grid.SetValue(_ActivePlayer, a);
                return true;
            }
            return false;
        }

    }
}
