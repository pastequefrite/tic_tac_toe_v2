﻿namespace TicTacToe.Models.Services.Authentication
{
    public class AuthenticationResponse
    {
        public string UserName { get; set; }
        public bool Authenticated { get; set; }
    }
}
