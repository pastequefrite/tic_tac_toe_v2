﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net.Mail;

namespace TicTacToe.Models.Services.User.Put
{
    public class UserRequest
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        public string NewPassword { get; set; }

        public string ConfirmNewPassword { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [EmailAddress(ErrorMessage = "Email Invalide")]
        public string Email { get; set; }

        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(UserName))
                throw new ArgumentNullException(nameof(UserName));

            if (string.IsNullOrWhiteSpace(Password))
                throw new ArgumentNullException(nameof(Password));

            if (!string.IsNullOrWhiteSpace(NewPassword))
            {
                if (NewPassword.Length < 8)
                    throw new Exception($"{nameof(NewPassword)} must contain at least 8 characters.");

                if (string.IsNullOrWhiteSpace(ConfirmNewPassword))
                    throw new ArgumentNullException(nameof(ConfirmNewPassword));

                if (Password.Equals(ConfirmNewPassword))
                    throw new Exception($"{nameof(NewPassword)} and {nameof(ConfirmNewPassword)} have to be equals.");
            }
            if (!string.IsNullOrWhiteSpace(Email))
            {
                try
                {   
                    new MailAddress(Email);
                }
                catch(FormatException)
                {
                    throw new Exception($"{nameof(Email)}");
                }
            }
        }
    }
}
