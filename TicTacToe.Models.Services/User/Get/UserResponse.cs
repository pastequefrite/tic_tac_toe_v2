﻿namespace TicTacToe.Models.Services.User.Get
{
    public class UserResponse
    {
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public int Win { get; set; }

        public int Lose { get; set; }

        public int Pat { get; set; }

        public virtual double WinRate
        {
            get
            {
                return (Win + Lose + Pat) == 0 ? 0 : Win / (double)(Win + Lose + Pat) * 100;
            }
        }
    }
}
