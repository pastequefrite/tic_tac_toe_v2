﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net.Mail;

namespace TicTacToe.Models.Services.User.Post
{
    public class UserRequest
    {
        [Required]  
        public string UserName { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(UserName))
                throw new ArgumentNullException(nameof(UserName));

            if (string.IsNullOrWhiteSpace(Password))
                throw new ArgumentNullException(nameof(Password));

            if (Password.Length < 8)
                throw new Exception($"{nameof(Password)} must contain at least 8 characters.");

            if (string.IsNullOrWhiteSpace(ConfirmPassword))
                throw new ArgumentNullException(nameof(ConfirmPassword));

            if (!Password.Equals(ConfirmPassword))
                throw new Exception($"{nameof(Password)} and {nameof(ConfirmPassword)} have to be equals.");

            if (!string.IsNullOrWhiteSpace(Email))
            {
                try
                {   
                    new MailAddress(Email);
                }
                catch(FormatException)
                {
                    throw new Exception($"{nameof(Email)}");
                }
            }
        }
    }
}
