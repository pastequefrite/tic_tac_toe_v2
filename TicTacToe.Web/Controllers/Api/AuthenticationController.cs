﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Composition;
using System.Threading.Tasks;
using TicTacToe.Interfaces.Bll.Contracts;
using TicTacToe.Models.Services.Authentication;
using TicTacToe.Web.Controllers.Api.ModelBinder.Authentication;

namespace TicTacToe.Web.Controllers.Api
{
    [Route("Api/[Controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        #region Private Property
        private readonly IAuthenticationContract contract;
        #endregion

        #region Contructor
        [ImportingConstructor]
        public AuthenticationController(IAuthenticationContract contract)
        {
            if (contract == null)
                throw new ArgumentNullException(nameof(contract));

            this.contract = contract;
        }

        #endregion

        #region Public Method
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<AuthenticationResponse> Get([ModelBinder(typeof(GetModelBinderFromUri))]AuthenticationRequest request)
        {
            return await contract.AuthenticationAsync(request);
        }
        #endregion
    }
}
