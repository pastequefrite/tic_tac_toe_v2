﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Models.Services.Authentication;

namespace TicTacToe.Web.Controllers.Api.ModelBinder.Authentication
{
    public class GetModelBinderFromUri : IModelBinder
    {
        Task IModelBinder.BindModelAsync(ModelBindingContext bindingContext)
        {
            return Task.Factory.StartNew(() =>
            {
                if (bindingContext == null)
                    throw new ArgumentNullException(nameof(bindingContext));

                var request = new AuthenticationRequest();

                var userName = bindingContext.ValueProvider.GetValue(nameof(AuthenticationRequest.UserName));
                request.UserName = userName.Values;

                var password = bindingContext.ValueProvider.GetValue(nameof(AuthenticationRequest.Password));
                request.Password = password.Values;

                //bindingContext.Model = request;
                bindingContext.Result = ModelBindingResult.Success(request);

                return Task.CompletedTask;
            });
        }
    }
}
