﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Threading.Tasks;
using TicTacToe.Models.Services.User.Get;

namespace TicTacToe.Web.Controllers.Api.ModelBinder.User
{
    public class GetModelBinderFromUri : IModelBinder
    {
        Task IModelBinder.BindModelAsync(ModelBindingContext bindingContext)
        {
            return Task.Factory.StartNew(() =>
                {
                    if (bindingContext == null)
                        throw new ArgumentNullException(nameof(bindingContext));

                    var request = new UserRequest();

                    var value = bindingContext.ValueProvider.GetValue(nameof(UserRequest.UserName));
                    request.UserName = value.Values;

                    //bindingContext.Model = request;
                    bindingContext.Result = ModelBindingResult.Success(request);

                    return Task.CompletedTask;
                });
        }
    }
}
