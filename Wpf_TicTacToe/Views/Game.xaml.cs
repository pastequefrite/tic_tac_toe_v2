﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Wpf_TicTacToe.ViewModels;

namespace Wpf_TicTacToe.Views
{
    /// <summary>
    /// Logique d'interaction pour Game.xaml
    /// </summary>
    public partial class Game : Page
    {

        GameViewModel _gameViewModel = new GameViewModel();

        public Game()
        {
            InitializeComponent();
            DataContext = _gameViewModel;
        }

        private void ButtonReplay_Click(object sender, RoutedEventArgs e)
        {
            _gameViewModel.InitGame();
        }

        private void ButtonQuit_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri(@"Views\Menu.xaml", UriKind.Relative));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _gameViewModel.CaseClick((Button)sender);
        }
    }
}