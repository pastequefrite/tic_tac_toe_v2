﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Wpf_TicTacToe.ViewModels;

namespace Wpf_TicTacToe.Views
{
    /// <summary>
    /// Logique d'interaction pour Stats.xaml
    /// </summary>
    public partial class Stats : Page
    {
        StatsViewModel _statsViewModel = new StatsViewModel();

        public Stats()
        {
            InitializeComponent();
            DataContext = _statsViewModel;
        }

        private void ButtonHome_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri(@"Views\Menu.xaml", UriKind.Relative));
        }
    }
}
