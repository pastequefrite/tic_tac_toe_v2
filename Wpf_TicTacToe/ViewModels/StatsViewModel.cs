﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf_TicTacToe.Models;
using Newtonsoft.Json;
using System.IO;
using System.Windows;

namespace Wpf_TicTacToe.ViewModels
{
    public class StatsViewModel : BaseModel
    {
        //Variables privées
        string _path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\stats.json";

        //Variables publiques
        public Uri UriBackground
        {
            get => new Uri("Resources/Pictures/gameBackground.png", UriKind.Relative);
        }
        public List<Stat> ListStat { get; set; }
        public StatsViewModel()
        {
            ListStat = GetStats();
        }

        public List<Stat> GetStats()
        {
            return GetFileContent(_path);
        }

        /// <summary>
        /// Lit le fichier entré en paramètre et retourne son contenu sinon null
        /// </summary>
        /// <param name="filePath">Répertoire du fichier</param>
        /// <returns>Liste d'objets stat</returns>
        private List<Stat> GetFileContent(string filePath)
        {
            try
            {
                if (!File.Exists(filePath))
                    File.Create(filePath).Close();
                

                return JsonConvert.DeserializeObject<List<Stat>>(File.ReadAllText(filePath));
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erreur : " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Modifie les statistiques selon les données entrées
        /// </summary>
        /// <param name="userWin">Utilisateur qui a gagné la partie</param>
        /// <param name="userLose">Utilisateur qui a perdu la partie</param>
        public void AddStatToFile(User userWin, User userLose)
        {
            try
            {
                //Récupération du contenu du fichier
                var list = GetFileContent(_path);

                if (list == null)
                {
                    list = new List<Stat>();
                }

                Stat userStatBddWin = list.FirstOrDefault(u => u.NomUser.ToUpper() == userWin.Nom.ToUpper());
                Stat userStatBddLose = list.FirstOrDefault(u => u.NomUser.ToUpper() == userLose.Nom.ToUpper());

                if (userStatBddWin != null)
                    userStatBddWin.Victoire++;
                else
                    list.Add(new Stat
                    {
                        NomUser = userWin.Nom,
                        Victoire = 1,
                        Defaite = 0
                    });

                if (userStatBddLose != null)
                    userStatBddLose.Defaite++;
                else
                    list.Add(new Stat
                    {
                        NomUser = userLose.Nom,
                        Victoire = 0,
                        Defaite = 1
                    });

                if (File.Exists(_path))
                    File.Delete(_path);
                
                File.WriteAllText(_path, JsonConvert.SerializeObject(list));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur : " + ex.Message);
            }
        }
    }
}
