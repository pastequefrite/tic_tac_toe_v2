﻿using System.ComponentModel;
using System.Windows;
using Wpf_TicTacToe.Hub;

namespace Wpf_TicTacToe
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            ClientHub.DisconnectAsync();
        }
    }
}
