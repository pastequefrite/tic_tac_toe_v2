﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Common;
using TicTacToe.Models.Services.Battle.Post;
using TicTacToe.Models.Services.Hubs;

namespace TicTacToe.Modules.SignalRGame.Hubs
{
    public class GameHub : Hub
    {
        protected static Dictionary<string, string> ListPlayer;

        public GameHub()
        {
            if (ListPlayer == null)
                ListPlayer = new Dictionary<string, string>();
        }

        public void Connect(string UserName)
        {
            var user = ListPlayer.SingleOrDefault(e => e.Key == UserName);
            if (ListPlayer.ContainsKey(UserName))
                ListPlayer[UserName] = Context.ConnectionId;
            else
                ListPlayer.Add(UserName, Context.ConnectionId);
            
            Clients.Others.SendAsync("PlayerList", ListPlayer.Select(e => e.Key).ToList());
        }

        public void Disconnect()
        {
            ListPlayer.Remove(ListPlayer.SingleOrDefault(e => e.Value == Context.ConnectionId).Key);
            Clients.Others.SendAsync("PlayerList", ListPlayer.Select(e => e.Key).ToList());
        }

        public async Task RefleshList()
        {
            var username = ListPlayer.SingleOrDefault(e => e.Value == Context.ConnectionId).Key;
            var list = ListPlayer.Where(e => !e.Key.Equals(username)).Select(e => e.Key).ToList();
            await Clients.Caller.SendAsync("PlayerList", list);
        }

        public void FightRequest(string namePlayerCall)
        {
            var battle = new Battle
            {
                _IdBattle = Guid.NewGuid().ToString(),
                _NamePlayerAsk = ListPlayer.SingleOrDefault(e => e.Value == Context.ConnectionId).Key,
                _IdPlayerAsk = Context.ConnectionId,
                _NamePlayerCall = namePlayerCall,
                _IdplayerCall = ListPlayer[namePlayerCall]
            };

            Clients.Client(battle._IdplayerCall).SendAsync("AcceptBattle", battle);
        }

        public void FightResponse(Battle battle)
        {
            if (!battle._Accepted.HasValue || !battle._Accepted.Value)
            {
                Clients.Client(battle._IdplayerCall).SendAsync("Reject", battle._NamePlayerCall);
            }
            else
            {
                battle._ActivePlayer = battle._NamePlayerCall;
                Clients.Clients(battle._IdPlayerAsk, battle._IdplayerCall).SendAsync("BeginFight", battle);
            }
        }

        public void ChangePlayer(Battle battle)
        {
            battle.UpdateActivePlayer();
            Clients.Client(battle.IdActivePlayer()).SendAsync("ChangePlayerTurn", battle);
        }

        public async Task EndGame(Battle battle)
        {
            var a = await CallApi.PostAsync<BattleRequest>(new BattleRequest
            {
                User1 = battle._NamePlayerAsk,
                User2 = battle._NamePlayerCall,
                Result = battle.Result.Value
            }, "api/Battle");

            await Clients.Clients(battle._IdPlayerAsk, battle._IdplayerCall).SendAsync("EndFight", battle);
        }
    }
}
