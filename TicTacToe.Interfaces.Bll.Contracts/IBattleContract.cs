﻿using System.Threading.Tasks;
using Get = TicTacToe.Models.Services.Battle.Get;
using Post = TicTacToe.Models.Services.Battle.Post;

namespace TicTacToe.Interfaces.Bll.Contracts
{
    public interface IBattleContract
    {
        Task<Get.BattlesResponse> GetAsync(Get.BattleRequest request);

        Task PostAsync(Post.BattleRequest request);
    }
}