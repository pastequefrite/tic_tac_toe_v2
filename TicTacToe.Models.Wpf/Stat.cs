﻿using System;

namespace TicTacToe.Models.Wpf
{
    public class Stat : BaseModel
    {
        string _nomUser;
        int _victoire;
        int _defaite;

        /// <summary>
        /// Nom de l'utilisateur
        /// </summary>
        public string NomUser
        {
            get => _nomUser;
            set
            {
                _nomUser = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nombres de victoire de l'utilisateur
        /// </summary>
        public int Victoire
        {
            get => _victoire;
            set
            {
                _victoire = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Nombre de défaites de l'utilisateur
        /// </summary>
        public int Defaite
        {
            get => _defaite;
            set
            {
                _defaite = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Ratio victoires/defaites de l'utilisateur
        /// </summary>
        public Double Ratio
        {
            get => _victoire / (_victoire + (double)_defaite) * 100.0;
        }
    }
}
