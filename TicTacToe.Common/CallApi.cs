﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace TicTacToe.Common
{
    public class CallApi
    {
        public static async Task<bool> PostAsync<T>(T model, string requestUri)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri("https://localhost:44383/");

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.PostAsJsonAsync(requestUri, model);

                //Checking the response is successful or not which is sent using HttpClient
                return Res.IsSuccessStatusCode;
            }
        }
    }
}