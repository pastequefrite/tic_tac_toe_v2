﻿using System;
using System.Composition;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Common;
using TicTacToe.Entity;
using TicTacToe.Interfaces.Bll.Contracts;
using Delete = TicTacToe.Models.Services.User.Delete;
using Get = TicTacToe.Models.Services.User.Get;
using Post = TicTacToe.Models.Services.User.Post;
using Put = TicTacToe.Models.Services.User.Put;

namespace TicTacToe.Modules.Bll.Contracts
{
    [Export(typeof(IUserContract))]
    public class UserContract : IUserContract
    {
        #region Private Property
        private readonly DataContext context;
        #endregion

        #region Constructor
        /// <summary>
        /// Controller Contract
        /// </summary>
        [ImportingConstructor]
        public UserContract(DataContext context)
        {
            this.context = context;
        }
        #endregion


        #region Public Method
        public Task<Get.UsersResponse> GetAsync(Get.UserRequest request)
        {
            return Task.Factory.StartNew(() =>
            {
                return Get(request);
            });
        }

        public Task PostAsync(Post.UserRequest request)
        {
            return Task.Factory.StartNew(() =>
            {
                Post(request);
            });

        }

        public Task PutAsync(Put.UserRequest request)
        {
            return Task.Factory.StartNew(() =>
            {
                Put(request);
            });

        }

        public Task DeleteAsync(Delete.UserRequest request)
        {
            return Task.Factory.StartNew(() =>
            {
                Delete(request);
            });

        }
        #endregion


        #region Private Method
        private Get.UsersResponse Get(Get.UserRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var users = context.User.Select(e => e);

            if (!string.IsNullOrWhiteSpace(request.UserName))
                users = users.Where(u => u.UserName == request.UserName);

            return new Get.UsersResponse
            {
                Users = users.Select(e => new Get.UserResponse
                {
                    UserName = e.UserName,
                    FirstName = e.FirstName,
                    LastName = e.LastName,
                    Email = e.Email,
                    Win = e.Win,
                    Lose = e.Lose,
                    Pat = e.Pat
                }).ToArray()
            };
        }

        private void Post(Post.UserRequest request)
        {
            request.Validate();

            if (context.User.Where(e => e.UserName == request.UserName).Any())
                throw new Exception($"The User {request.UserName} already exists.");

            context.User.Add(new Models.UserModel
            {
                UserName = request.UserName,
                Password = CommonTools.GetSHA512Hash(request.Password),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            });
            context.SaveChanges();
        }

        private void Put(Put.UserRequest request)
        {
            var user = context.User.SingleOrDefault(e => e.UserName == request.UserName);

            if (user == null)
                throw new Exception($"The User {request.UserName} doesn't exist.");

            if (!string.IsNullOrWhiteSpace(request.UserName)) user.UserName = request.UserName;
            if (!string.IsNullOrWhiteSpace(request.Password)) user.Password = CommonTools.GetSHA512Hash(request.Password);
            if (!string.IsNullOrWhiteSpace(request.FirstName)) user.FirstName = request.FirstName;
            if (!string.IsNullOrWhiteSpace(request.LastName)) user.LastName = request.LastName;
            if (!string.IsNullOrWhiteSpace(request.Email)) user.Email = request.Email;

            context.SaveChanges();
        }

        private void Delete(Delete.UserRequest request)
        {
            var user = context.User.SingleOrDefault(e => e.UserName == request.UserName);

            if (user == null)
                throw new Exception($"The User {request.UserName} doesn't exist.");

            context.User.Remove(user);
            context.SaveChanges();
        }
        #endregion
    }
}
